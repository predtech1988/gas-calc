module gasPrice

go 1.18

require github.com/gorilla/mux v1.8.0

require (
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
)
