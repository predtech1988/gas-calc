package models

import (
	json "encoding/json"
	"strings"
	"time"
)

type JSONTime time.Time

//easyjson:json
type History struct {
	Ethereum struct {
		Transactions []Transaction `json:"transactions"`
	} `json:"ethereum"`
}

//easyjson:json
type Transaction struct {
	Time           JSONTime `json:"time"`
	GasPrice       float64  `json:"gasPrice"`
	GasValue       float64  `json:"gasValue"`
	Average        float64  `json:"average"`
	MaxGasPrice    float64  `json:"maxGasPrice"`
	MedianGasPrice float64  `json:"medianGasPrice"`
}

//easyjson:json
type Result struct {
	GasSpentByMonth []Month     `json:"gasSpentByMonth"`
	DailyAvgPrice   []Daily     `json:"dailyAvgPrice"`
	Frequency       []Frequency `json:"frequency"`
	TotalSpentOnGas float64     `json:"totalSpentOnGas"`
}

//easyjson:json
type Month struct {
	Month    string  `json:"month"`
	GasSpent float64 `json:"gasSpent"`
}

//easyjson:json
type Daily struct {
	Date     string  `json:"date"`
	AvgValue float64 `json:"avgValue"`
}

//easyjson:json
type Frequency struct {
	PriceDistribution string `json:"priceDistribution"`
	Entries           int    `json:"hours"`
}

func (j *JSONTime) UnmarshalJSON(b []byte) error {
	origTime := strings.Trim(string(b), "\"")
	parsedTime, err := time.Parse("06-01-02 15:04", origTime)
	if err != nil {
		return err
	}
	*j = JSONTime(parsedTime)

	return nil
}

func (j JSONTime) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Time(j))
}

func (j JSONTime) Value() time.Time {
	return time.Time(j)
}
