package service

import (
	"errors"
	"fmt"
	"gasPrice/internal/models"
	model "gasPrice/internal/models"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/mailru/easyjson"
)

const filePermission = 0o600

type PeriodType uint8

const (
	Day PeriodType = iota
	Month
	Hour
)

func getHistoryData(url string) (*model.History, error) {
	res, err := http.Get(url) //nolint:gosec
	if err != nil {
		return nil, fmt.Errorf("cant't download file! %s; %w", url, err)
	}
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("cant't get file! %s; %w", url, err)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("cant't parse file!; %w", err)
	}
	defer res.Body.Close()

	var history model.History
	err = easyjson.Unmarshal(body, &history)
	if err != nil {
		return nil, fmt.Errorf("cant't unmarshal body; %w", err)
	}

	return &history, nil
}

func splitPeriod(transactions []models.Transaction, periodType PeriodType, res *model.Result) error {
	switch periodType {
	case Month:
		return splitByMonth(transactions, res)
		// case Day:
		// case Hour:
	}

	return errors.New("unknown period")
}

func splitByMonth(transactions []models.Transaction, res *model.Result) error {
	var sum float64
	for i := 0; i < len(transactions); {
		periodSize := getPeriodSize(transactions[i:]) // period between 1st and 2nd transaction (in our case 1 hour)
		startTime := transactions[i].Time.Value()
		StartPeriod := time.Date(startTime.Year(), startTime.Month(), 1, 0, 0, 0, 0, time.UTC) // 1 day of the month
		nextPeriod := StartPeriod.AddDate(0, 1, 0)                                             // start period + 1 day
		var xxx = nextPeriod.Sub(startTime) / periodSize                                       // next month start in xxx element in array
		newI := i + int(xxx)                                                                   // index of the 1st entry of next Month
		sum = gasSpentByMonth(transactions[i:newI])                                            // Previous month 1st-last entry's
		month := model.Month{
			Month:    transactions[i].Time.Value().Month().String(),
			GasSpent: sum,
		}
		res.GasSpentByMonth = append(res.GasSpentByMonth, month)

		i = newI
	}

	return nil
}

func getPeriodSize(transactions []models.Transaction) time.Duration {
	return transactions[1].Time.Value().Sub(transactions[0].Time.Value())
}
