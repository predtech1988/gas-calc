package service

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

type service struct {
	router        *mux.Router
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
}

func (srv *service) StartService() error {
	err := http.ListenAndServe(":8080", srv.router) //nolint: gosec
	if err != nil {
		return fmt.Errorf("cant't start server %w", err)
	}

	return nil
}

type responseWriter struct {
	http.ResponseWriter
	code int
}

func (w *responseWriter) WriteHeader(statusCode int) {
	w.code = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}

const logPermissions = 0o600

func NewService() *service {
	file, err := os.OpenFile("./logs/logs.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, logPermissions)
	if err != nil {
		log.Fatal(err)
	}

	srv := &service{
		router:        mux.NewRouter(),
		WarningLogger: log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile),
		InfoLogger:    log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile),
		ErrorLogger:   log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile),
	}
	srv.registerHandlers()

	return srv
}

func (srv *service) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rw := &responseWriter{w, http.StatusOK}
		logEntity := fmt.Sprintf("%s %s %s", "started", r.Method, r.RequestURI)
		srv.InfoLogger.Println(logEntity)
		start := time.Now()
		next.ServeHTTP(rw, r)
		const (
			serverError = 500
			userError   = 400
			completed   = "completed"
		)
		logLine := fmt.Sprintf("%s %s %s %d %s %d %s", r.Method, r.RequestURI, "statusCode:",
			rw.code, "took:", time.Since(start).Milliseconds(), "ms")
		switch {
		case rw.code >= serverError:
			srv.ErrorLogger.Println(completed, logLine)
		case rw.code >= userError:
			srv.WarningLogger.Println(completed, logLine)
		default:
			srv.InfoLogger.Println(completed, logLine)
		}
	})
}

func (srv *service) registerClientHandlers() {
	srv.router.HandleFunc("/", srv.handleGasStats()).Methods(http.MethodGet)
	srv.router.HandleFunc("/spent-by-month", srv.handleSpentByMonth()).Methods(http.MethodGet)
	srv.router.HandleFunc("/daily-average-price", srv.handleDailyAveragePrice()).Methods(http.MethodGet)
	srv.router.HandleFunc("/price-frequency", srv.handlePriceFrequency()).Methods(http.MethodGet)
	srv.router.HandleFunc("/total-spent-on-gas", srv.handleTotalSpentOnGas()).Methods(http.MethodGet)
}

func (srv *service) registerHandlers() {
	srv.router.Use(srv.logRequest)
	srv.registerClientHandlers()
	srv.InfoLogger.Println("Handlers are registered")
}
