package service

import (
	"fmt"
	"gasPrice/internal/models"
	model "gasPrice/internal/models"
	"sort"
)

const hourWindow = 24 // 24 hour's in a day (24 object)

func dailyAveragePrice(gas *model.History, res *model.Result) {
	// We can do it faster by making goroutine, but we lose order
	for i := 0; i < len(gas.Ethereum.Transactions); {
		calcDaily(gas.Ethereum.Transactions[i:i+hourWindow], res)
		i += hourWindow
	}
}

func gasSpentByMonth(window []model.Transaction) float64 {
	var sum float64
	for _, elem := range window {
		sum += elem.GasValue
	}

	return sum
}

func calcDaily(window []model.Transaction, res *model.Result) {
	var sum float64
	for _, v := range window {
		sum += v.GasPrice
	}
	result := models.Daily{
		Date:     window[0].Time.Value().Format("2006-01-02"),
		AvgValue: sum / float64(len(window)),
	}
	res.DailyAvgPrice = append(res.DailyAvgPrice, result)
}

func spentOnGas(gas *model.History, res *model.Result) {
	spent := make(chan float64)
	var (
		counter    int
		totalSpent float64
	)
	for i := 0; i < len(gas.Ethereum.Transactions); i += hourWindow {
		window := gas.Ethereum.Transactions[i : i+hourWindow]
		go calcSpentOnGas(&window, spent)
		counter++
	}
	for i := 0; i < counter; i++ {
		totalSpent += <-spent
	}
	res.TotalSpentOnGas = totalSpent
}

func calcSpentOnGas(window *[]model.Transaction, chTotal chan float64) {
	var sumTotal float64
	for _, v := range *window {
		sumTotal += v.GasPrice * v.GasValue
	}
	chTotal <- sumTotal
}

func priceFreq(gas *model.History, res *model.Result) {
	priceFreq := prepareMap()
	freq := populatePrice(gas.Ethereum.Transactions, priceFreq)
	for k, v := range freq {
		filed := formatKey(k)
		record := model.Frequency{
			PriceDistribution: filed,
			Entries:           v,
		}
		res.Frequency = append(res.Frequency, record)
	}
}

func prepareMap() map[int]int {
	// Populates dictionary with price distribution keys
	data := make(map[int]int)

	for i := 0; i <= 250; i += 10 {
		data[i] = 0
	}

	// Add more values for price representation
	data[300] = 0
	data[400] = 0
	data[500] = 0
	data[1000] = 0

	return data
}

func populatePrice(window []model.Transaction, data map[int]int) map[int]int {
	// Dirty and slow, nested loop + sort
	allKeys := make([]int, 0, len(data))

	// Sorting keys
	for k, _ := range data {
		allKeys = append(allKeys, k)
	}
	sort.Slice(allKeys, func(i, j int) bool {
		return allKeys[i] > allKeys[j]
	})

	// Populate data
	for _, hour := range window {
		for _, k := range allKeys {
			if hour.MedianGasPrice > float64(k) {
				data[k] += 1
				break
			}
		}
	}
	fmt.Println(data)
	return data
}

func formatKey(i int) string {
	// Formats intervals for the PriceDistribution filed
	switch {
	case i >= 1000:
		return "500-1000"
	case i >= 500:
		return "400-500"
	case i >= 400:
		return "300-400"
	case i >= 300:
		return "250-300"
	case i > 250:
		return "240-250"
	}

	if i <= 250 {
		return fmt.Sprintf("%d-%d", i, i+10)
	}

	return ""
}
