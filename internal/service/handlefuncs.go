package service

import (
	"encoding/json"
	"errors"
	model "gasPrice/internal/models"
	"net/http"

	"github.com/mailru/easyjson"
)

const (
	url      = "https://raw.githubusercontent.com/CryptoRStar/GasPriceTestTask/main/gas_price.json"
	cont     = "Content-Type"
	contType = "application/json"
)

var ErrMarshal = errors.New("can't marshal data")

func (srv *service) handleGasStats() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// In this handler we don't save json locally
		gas, err := getHistoryData(url)
		if err != nil {
			srv.WarningLogger.Printf("error while preparing file; %s", err.Error())

			return
		}
		var res model.Result
		// 1) Сколько было потрачено gas помесячно.
		splitPeriod(gas.Ethereum.Transactions, Month, &res)
		// 2) Среднюю цену gas за день.
		dailyAveragePrice(gas, &res) // OK
		// 3) Частотное распределение цены по часам(за весь период).
		priceFreq(gas, &res)
		// 4) Сколько заплатили за весь период (gas price * value).
		spentOnGas(gas, &res)

		easyjson.MarshalToHTTPResponseWriter(res, w)
	}
}

//				Separate handles
func (srv *service) handleSpentByMonth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		gas, err := getHistoryData(url)
		if err != nil {
			srv.WarningLogger.Printf("error while preparing file; %s", err.Error())

			return
		}
		var res model.Result
		splitPeriod(gas.Ethereum.Transactions, Month, &res)
		data, err := json.MarshalIndent(res.GasSpentByMonth, "", "\t")
		if err != nil {
			srv.ErrorLogger.Println(ErrMarshal.Error())
		}
		w.Header().Set(cont, contType)
		w.Write(data)
	}
}

func (srv *service) handleDailyAveragePrice() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		gas, err := getHistoryData(url)
		if err != nil {
			srv.WarningLogger.Printf("error while preparing file; %s", err.Error())

			return
		}
		var res model.Result
		dailyAveragePrice(gas, &res)
		data, err := json.MarshalIndent(res.DailyAvgPrice, "", "\t")
		if err != nil {
			srv.ErrorLogger.Println(ErrMarshal.Error())
		}
		w.Header().Set(cont, contType)
		w.Write(data)
	}
}

func (srv *service) handlePriceFrequency() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		gas, err := getHistoryData(url)
		if err != nil {
			srv.WarningLogger.Printf("error while preparing file; %s", err.Error())

			return
		}
		var res model.Result
		priceFreq(gas, &res)
		data, err := json.MarshalIndent(res.Frequency, "", "\t")
		if err != nil {
			srv.ErrorLogger.Println(ErrMarshal.Error())
		}
		w.Header().Set(cont, contType)
		w.Write(data)
	}
}

func (srv *service) handleTotalSpentOnGas() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		gas, err := getHistoryData(url)
		if err != nil {
			srv.WarningLogger.Printf("error while preparing file; %s", err.Error())

			return
		}
		var res model.Result
		spentOnGas(gas, &res)
		data, err := json.MarshalIndent(res.TotalSpentOnGas, "", "\t")
		if err != nil {
			srv.ErrorLogger.Println(ErrMarshal.Error())
		}
		w.Header().Set(cont, contType)
		w.Write([]byte("TotalSpentOnGas:"))
		w.Write(data)
	}
}
