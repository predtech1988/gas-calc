package main

import "gasPrice/internal/service"

func main() {
	srv := service.NewService()
	err := srv.StartService()
	if err != nil {
		srv.ErrorLogger.Fatalln("Can't start gateway api", err.Error())
	}
}
